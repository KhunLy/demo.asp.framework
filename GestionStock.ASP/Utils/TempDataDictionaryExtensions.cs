﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionStock.ASP.Utils
{
    public static class TempDataDictionaryExtensions
    {
        public static void Info(this TempDataDictionary tempData, string message)
        {
            tempData["info"] = message;
        }

        public static void Success(this TempDataDictionary tempData, string message)
        {
            tempData["success"] = message;
        }

        public static void Error(this TempDataDictionary tempData, string message)
        {
            tempData["error"] = message;
        }
    }
}