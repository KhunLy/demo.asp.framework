﻿using GestionStock.DAL.Abstractions.Enums;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Utils
{
    public static class SessionExtensions
    {
        public static int? UserId(this HttpSessionStateBase session)
        {
            return session[nameof(UserId)] as int?;
        }

        public static string UserEmail(this HttpSessionStateBase session)
        {
            return session[nameof(UserEmail)] as string;
        }

        public static UserRole? UserRole(this HttpSessionStateBase session)
        {
            return session[nameof(UserRole)] as UserRole?;
        }

        public static bool HasAnyRole(this HttpSessionStateBase session, params UserRole[] roles)
        {
            return roles.ToList().Exists(r => session.UserRole() == r);
        }

        public static bool IsLogged(this HttpSessionStateBase session)
        {
            return session[nameof(IsLogged)] as bool? ?? false;
        }

        public static void Start(this HttpSessionStateBase session, int id, string email, UserRole role)
        {
            session[nameof(IsLogged)] = true;
            session[nameof(UserEmail)] = email;
            session[nameof(UserId)] = id;
            session[nameof(UserRole)] = role;
        }
    }
}