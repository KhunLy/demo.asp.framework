﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionStock.ASP.Utils
{
    public static class SelectUtils
    {
        public static IEnumerable<SelectListItem> GetSelectFromEnum<T>(bool emptyLine = false)
            where T : Enum
        {

            IEnumerable<SelectListItem> lines = Enum.GetValues(typeof(T)).Cast<T>()
                    .Select(s => new SelectListItem
                    {
                        Value = s.ToString(),
                        Text = s.ToString(),
                    });

            return emptyLine ? GetOneEmptyLine().Concat(lines) : lines;
        }

        public static IEnumerable<SelectListItem> GetOneEmptyLine(string text = " - ")
        {
            yield return new SelectListItem { Text = text, Value = "" };
        }
    }
}