﻿using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace GestionStock.ASP.Utils
{
    public class AuthorizationAttribute : AuthorizeAttribute
    {
        private readonly UserRole[] _authorizedRoles;

        public AuthorizationAttribute(params UserRole[] authorizedRoles)
        {
            _authorizedRoles = authorizedRoles;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if(!filterContext.HttpContext.Session.HasAnyRole(_authorizedRoles))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary 
                {
                    { "Area", "Security" },
                    { "Controller", "Authentication" },
                    { "Action", "Login" },
                });
            }
        }
    }
}