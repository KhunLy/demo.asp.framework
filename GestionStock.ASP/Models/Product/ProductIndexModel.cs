﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Product
{
    public class ProductIndexModel : IndexModelBase<ProductModel>
    {
        public string Search { get; set; }
        public ProductIndexModel()
        {

        }
        public ProductIndexModel(IEnumerable<ProductModel> results)
            : base(results)
        {

        }
    }
}