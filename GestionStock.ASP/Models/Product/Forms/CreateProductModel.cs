﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Product.Forms
{
    public class CreateProductModel
    {
        [Required]
        [StringLength(100, MinimumLength =4)]
        [Display(Name = "Nom")]
        public string Name { get; set; }

        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Range(0, 9999.99)]
        [Display(Name = "Prix")]
        public decimal Price { get; set; }

        [Required]
        [Range(0, 9999999)]
        public int Stock { get; set; }
    }
}