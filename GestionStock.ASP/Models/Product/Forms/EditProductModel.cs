﻿using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Models.Product.Forms
{
    public class EditProductModel
    {
        [Display(Name = "Référence")]
        public string Reference { get; set; } // display only

        [Required]
        [StringLength(100, MinimumLength = 4)]
        [Display(Name = "Nom")]
        public string Name { get; set; }

        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Range(0, 9999.99)]
        [Display(Name = "Prix")]
        public decimal Price { get; set; }

        public int Stock { get; set; }

        [Required]
        [Range(-9999999, 9999999)]
        [Display(Name = "Modification Stock")]
        public int UpdateStock { get; set; }
    }
}