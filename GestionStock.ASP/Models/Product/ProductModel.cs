﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Product
{
    public class ProductModel
    {
        [Display(Name = "Référence")]
        public string Reference { get; set; }
        [Display(Name = "Nom")]
        public string Name { get; set; }
        public int Stock { get; set; }
        [Display(Name = "Prix")]
        public decimal Price { get; set; }
        public string Label { get; set; }
    }
}