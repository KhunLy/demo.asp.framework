﻿using GestionStock.DAL.Abstractions.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Models.Order
{
    public class OrderModel
    {
        [Display(Name = "Référence")]
        public string Reference { get; set; }
        [Display(Name = "État")]
        public OrderStatus Status { get; set; }

        public DateTime Date { get; set; }
        [Display(Name = "Client")]
        public string CustomerRef { get; set; }
        public decimal Total { get; set; }
    }
}