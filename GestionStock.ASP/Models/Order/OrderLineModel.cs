﻿using GestionStock.ASP.Models.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Order
{
    public class OrderLineModel
    {
        public int Id { get; set; }

        [Display(Name = "Quantité")]
        public int Quantity { get; set; }

        [Display(Name = "Produit")]
        public string ProductRef { get; set; }

        public string OrderRef { get; set; }

        [Display(Name = "Prix unitaire")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Prix")]
        public decimal LinePrice { get; set; }
    }
}