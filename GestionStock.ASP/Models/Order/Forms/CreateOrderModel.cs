﻿using GestionStock.ASP.Models.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionStock.ASP.Models.Order.Forms
{
    public class CreateOrderModel
    {
        [Required]
        [Display(Name = "Client")]
        public string CustomerRef { get; set; }
        public IEnumerable<SelectListItem> AllCustomers { get; set; }
    }
}