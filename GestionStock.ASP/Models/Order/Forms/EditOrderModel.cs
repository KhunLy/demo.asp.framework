﻿using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Models.Order.Forms
{
    public class EditOrderModel
    {
        #region Display only
        [Display(Name = "Référence")]
        public string Reference { get; set; }

        [Display(Name = "Client")]
        public string CustomerRef { get; set; }

        public decimal Total { get; set; }

        [Display(Name = "État")]
        public OrderStatus Status { get; set; }

        public DateTime Date { get; set; }

        public List<OrderLineModel> OrderLines { get; set; }

        #endregion

        #region Form for adding new Line
        public AddOrderLineModel NewLine { get; set; } = new AddOrderLineModel(); 
        #endregion
    }
}