﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Order.Forms
{
    public class AddOrderLineModel
    {
        [Required]
        [Display(Name = "Produit")]
        public string ProductRef { get; set; }

        [Required]
        [Range(-9999999, 9999999)]
        [Display(Name = "Quantité")]
        public int Quantity { get; set; }
    }
}