﻿using GestionStock.ASP.Validators;
using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GestionStock.ASP.Models.Order
{
    public class OrderIndexModel : IndexModelBase<OrderModel>
    {
        public OrderIndexModel()
        {

        }
        public OrderIndexModel(IEnumerable<OrderModel> results)
            : base(results)
        {

        }

        [Display(Name = "Référence")]
        public string Reference { get; set; }
        [Display(Name = "Client")]
        public string CustomerRef { get; set; }
        [Display(Name = "États")]
        public List<OrderStatus> SelectedStatuses { get; set; } = new List<OrderStatus>() 
        {
            OrderStatus.InProgress,
            OrderStatus.Pending,
        };

        [DataType(DataType.Date)]
        [Display(Name = "Entre le")]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "et le")]
        [DateComparerValidator(nameof(FromDate))]
        public DateTime? ToDate { get; set; }

        public IEnumerable<SelectListItem> AllCustomers { get; set; }
        public IEnumerable<SelectListItem> AllStatuses { get; set; }
    }
}