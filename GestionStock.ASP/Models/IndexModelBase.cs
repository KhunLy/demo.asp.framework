﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models
{
    public abstract class IndexModelBase<T>
    {
        public int Count { get; set; }
        public int Offset { get; set; } = 0;

        [Range(1, 100)]
        public int Limit { get; set; } = 10;
        public IEnumerable<T> Results { get; set; }

        public IndexModelBase()
        {

        }

        public IndexModelBase(IEnumerable<T> results)
        {
            Results = results;
        }
    }
}