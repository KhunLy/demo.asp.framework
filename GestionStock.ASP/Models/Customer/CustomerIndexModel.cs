﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Customer
{
    public class CustomerIndexModel : IndexModelBase<CustomerModel>
    {
        public string Search { get; set; }
        public CustomerIndexModel()
        {

        }
        public CustomerIndexModel(IEnumerable<CustomerModel> results) : base(results)
        {
        }
    }
}