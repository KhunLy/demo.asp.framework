﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Customer
{
    public class CustomerModel
    {
        [Display(Name = "Référence")]
        public string Reference { get; set; }
        [Display(Name = "Nom")]
        public string LastName { get; set; }
        [Display(Name = "Prénom")]
        public string FirstName { get; set; }
        public string Email { get; set; }
    }
}