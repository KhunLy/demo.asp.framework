﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Customer.Forms
{
    public class CreateCustomerModel
    {
        [Required]
        [StringLength(50, MinimumLength = 2 )]
        [Display(Name = "Nom")]
        public string LastName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        [Display(Name = "Prénom")]
        public string FirstName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(25)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Tel")]
        public string Phone { get; set; }
    }
}