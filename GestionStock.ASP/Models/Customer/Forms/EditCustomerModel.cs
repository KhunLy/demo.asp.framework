﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models.Customer.Forms
{
    public class EditCustomerModel
    {
        [Display(Name = "Référence")]
        public string Reference { get; set; } // display only

        [Required]
        [EmailAddress]
        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(25)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Tel")]
        public string Phone { get; set; }
    }
}