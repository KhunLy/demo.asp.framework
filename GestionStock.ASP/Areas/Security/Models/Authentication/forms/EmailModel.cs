﻿using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Areas.Security.Models.Authentication.forms
{
    public class EmailModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}