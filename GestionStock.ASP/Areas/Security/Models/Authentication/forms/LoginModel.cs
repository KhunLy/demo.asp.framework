﻿using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Areas.Security.Models.Authentication.forms
{
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}