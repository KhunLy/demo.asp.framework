﻿using System.ComponentModel.DataAnnotations;

namespace GestionStock.ASP.Areas.Security.Models.Authentication.forms
{
    public class ResetPasswordModel
    {
        [Required]
        [RegularExpression(
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&#])[A-Za-z\\d@$!%*?&#]{8,}$", 
             ErrorMessage = "Votre mot de passe doit contenir au moins une minuscule, une majuscule, un chiffre, un caractère spécial et avoir une longueur minimal de 8 caractères"
        )]
        [Display(Name = "Nouveau mot de passe")]
        [DataType(DataType.Password)]
        public string NewPasssword { get; set; }

        [Required]
        [Compare(nameof(NewPasssword))]
        [Display(Name = "Confirmer votre mot de passe")]
        [DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }
    }
}