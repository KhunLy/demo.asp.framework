﻿using GestionStock.ASP.Areas.Security.Models.Authentication.forms;
using GestionStock.ASP.Utils;
using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Net.Mail;
using System.Web.Mvc;

namespace GestionStock.ASP.Areas.Security.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthService _loginService;

        public AuthenticationController(IAuthService loginService)
        {
            _loginService = loginService;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel form)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    UserBO user = _loginService.Login(form.Email, form.Password);
                    Session.Start(user.Id, user.Email, user.Role);
                    TempData.Info("Bienvenue");
                    if(user.Role == UserRole.Admin)
                    {
                        return RedirectToAction("Index", "Dashboard");
                    }
                    else if(user.Role == UserRole.Seller) 
                    {
                        return RedirectToAction("Index", "Customer");
                    }
                }
                catch(InvalidCredentialsException)
                {
                    TempData.Error("Invalid Credentials");
                }
            }
            return View(form);
        }

        public ActionResult AskResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AskResetPassword(EmailModel form)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _loginService.AskResetPassword(form.Email);
                    TempData.Success("Un email vous a été envoyé");
                    return RedirectToAction("Login");
                }
                catch (EntityNotFoundException)
                {
                    TempData.Error("Email introuvable");
                }
                catch (SmtpException)
                {
                    TempData.Error("Envoi email impossible");
                }
            }
            return View(form);
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost()]
        [Route("ResetPassword?token={token}")]
        public ActionResult ResetPassword(string token, ResetPasswordModel form)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _loginService.ResetPassword(token, form.NewPasssword);
                    TempData.Success("Votre mot de passe a été mis à jour");
                    return RedirectToAction("Login");
                }
                catch (Exception) // jwt Exception
                {
                    TempData.Error("Invalid token");
                }
            }
            return View(form);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }
    }
}