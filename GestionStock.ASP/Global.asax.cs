using GestionStock.ASP.DI;
using GestionStock.BLL.Configs;
using GestionStock.BLL.Mocks;
using GestionStock.BLL.Services;
using GestionStock.DAL;
using GestionStock.DAL.Abstractions.Interfaces;
using GestionStock.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Toolbox.Security.Interfaces;
using Toolbox.Security.Services;

namespace GestionStock.ASP
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            #region Dependency Injection
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            DIServiceResolver resolver = new DIServiceResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);
            #endregion
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region DAL
            services.AddScoped(sp => new GestionStockContext("name=default"));
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            #endregion

            #region SMTP
            services.AddSingleton(new MailConfig
            {
                Host = ConfigurationManager.AppSettings["SMTP_HOST"],
                Port = int.Parse(ConfigurationManager.AppSettings["SMTP_PORT"]),
                Email = ConfigurationManager.AppSettings["SMTP_EMAIL"],
                Password = ConfigurationManager.AppSettings["SMTP_PASSWORD"],
                IsDebug = ConfigurationManager.AppSettings["ENV"] == "DEBUG",
            });
            services.AddScoped<SmtpClient>();
            services.AddScoped<IMailService, MailService>();
            #endregion

            #region SECURITY
            services.AddScoped<HashAlgorithm, SHA512CryptoServiceProvider>();
            services.AddScoped<IHashService, HashService>();

            services.AddSingleton(new JwtConfig
            {
                Signature = ConfigurationManager.AppSettings["JWT_SIGNATURE"],
                Duration = int.Parse(ConfigurationManager.AppSettings["JWT_DURATION"]),
                ValidateLifeTime = bool.Parse(ConfigurationManager.AppSettings["JWT_VALIDATE_LIFE_TIME"])
            });
            services.AddScoped<JwtSecurityTokenHandler>();
            services.AddScoped<IJwtService, JwtService>();

            #endregion

            #region BLL
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IAuthService,AuthService >(); //TODO Not finished yet only login method working for now
            //services.AddScoped<IAuthService, FakeAuthService>();
            #endregion
        }
    }
}
