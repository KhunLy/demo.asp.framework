﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GestionStock.ASP.DI
{
    public class DIServiceResolver : IDependencyResolver
    {
        private readonly IServiceProvider _serviceProvider;

        public IServiceScope CreateScope()
        {
            return _serviceProvider.CreateScope();
        }

        public DIServiceResolver(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public object GetService(Type serviceType)
        {
            return _serviceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _serviceProvider.GetServices(serviceType);
        }
    }
}