﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace GestionStock.ASP.DI
{
    public class DIControllerFactory: DefaultControllerFactory
    {
        private IServiceScope _scope;
        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            Type controllerType = Assembly.GetExecutingAssembly()
                .GetTypes().FirstOrDefault(t => t.IsSubclassOf(typeof(Controller)) && t.Name == controllerName + "Controller");
            _scope = (DependencyResolver.Current as DIServiceResolver).CreateScope();
            return _scope.ServiceProvider.GetService(controllerType) as IController;
        }

        public override void ReleaseController(IController controller)
        {
            _scope.Dispose();
            base.ReleaseController(controller);
        }
    }
}