﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GestionStock.ASP.DI
{
    public static class DIExtensions
    {
        public static void AddControllers(this IServiceCollection services)
        {
            services.AddTransient<IControllerFactory, DIControllerFactory>();
            IEnumerable<Type> controllerTypes = Assembly.GetExecutingAssembly()
                .GetTypes().Where(t => t.IsSubclassOf(typeof(Controller)));
            foreach (Type controllerType in controllerTypes)
            {
                services.AddTransient(controllerType);
            }
        }
    }
}