﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Validators
{
    public class DateComparerValidatorAttribute : ValidationAttribute
    {
        public string DatePropertyComparer { get; }

        public DateComparerValidatorAttribute(string datePropertyComparer)
        {
            DatePropertyComparer = datePropertyComparer;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null)
            {
                return null;
            }
            object model = validationContext.ObjectInstance;
            DateTime? dateBefore = model.GetType().GetProperty(DatePropertyComparer).GetValue(model) as DateTime?;
            if(dateBefore == null)
            {
                return null;
            }
            if(dateBefore <= (value as DateTime?))
            {
                return null;
            }
            return new ValidationResult("La date de début doit être supérieure à la date de fin");
        }
    }
}