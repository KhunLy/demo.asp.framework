﻿using GestionStock.ASP.Utils;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System.Web.Mvc;

namespace GestionStock.ASP.Controllers
{
    [Authorization(UserRole.Admin)]
    public class DashboardController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;

        public DashboardController(IOrderService orderService, IProductService productService)
        {
            _orderService = orderService;
            _productService = productService;
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StatusRepartitionPieChart()
        {
            return PartialView(_orderService.GetCountAndSumGroupByStatuses());
        }

        public ActionResult Stock()
        {
            return PartialView(_productService.FindOrderByQuantity(50));
        }
    }
}