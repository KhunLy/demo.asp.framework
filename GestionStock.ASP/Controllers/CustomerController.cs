﻿using GestionStock.ASP.Models.Customer;
using GestionStock.ASP.Models.Customer.Forms;
using GestionStock.ASP.Utils;
using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System.Linq;
using System.Web.Mvc;

namespace GestionStock.ASP.Controllers
{
    [Authorization(UserRole.Admin, UserRole.Seller)]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public ActionResult Index(CustomerIndexModel model)
        {
            model.Results = _customerService.FindWithFilters(model.Search, model.Offset, model.Limit)
                .Select(c => new CustomerModel
                {
                    Reference = c.Reference,
                    LastName = c.LastName,
                    FirstName = c.FirstName,
                });
            model.Count = _customerService.CountWithFilters(model.Search);
            return View(model);
        }

        public ActionResult Details(string reference)
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateCustomerModel form)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    _customerService.Add(new CustomerBO(form.LastName, form.FirstName, form.Email, form.Phone));
                    TempData.Success("Ajout OK");
                    return RedirectToAction("Index");
                } 
                catch (UniqueFieldException ex)
                {
                    ModelState.AddModelError(ex.FieldName, "Cet email est déjà utilisé");
                }
            }
            return View(form);
        }

        public ActionResult Edit(string id)
        {
            try
            {
                CustomerBO bo = _customerService.FindOne(id);
                EditCustomerModel model = new EditCustomerModel()
                {
                    Reference = bo.Reference,
                    Email = bo.Email,
                    Phone = bo.Phone,
                };
                return View(model);
            }
            catch (EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, EditCustomerModel form)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _customerService.Edit(new CustomerBO(id, form.Email, form.Phone));
                    TempData.Success("Modification OK");
                    return RedirectToAction("Index");
                }
                catch (EntityNotFoundException)
                {
                    return new HttpNotFoundResult();
                }
                catch (UniqueFieldException ex)
                {
                    ModelState.AddModelError(ex.FieldName, "Cet email est déjà utilisé");
                }
            }
            return View(form);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Delete(string id)
        {
            try
            {
                _customerService.Delete(id);
                TempData.Success($"Le client ({id}) a été supprimé");
            }
            catch (PendingOrdersException)
            {
                TempData.Error("Impossible de supprimer ce client, il lui reste des commandes impayées");
            }
            catch(EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
            return RedirectToAction("Index");
        }
    }
}