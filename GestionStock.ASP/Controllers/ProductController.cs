﻿using GestionStock.ASP.Models.Product;
using GestionStock.ASP.Models.Product.Forms;
using GestionStock.ASP.Utils;
using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GestionStock.ASP.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [Authorization(UserRole.Admin, UserRole.Seller)]
        public ActionResult Index(ProductIndexModel model)
        {
            model.Results = _productService.FindWithFilters(model.Offset, model.Limit, model.Search)
                .Select(p => new ProductModel
                {
                    Reference = p.Reference,
                    Name = p.Name,
                    Price = p.Price,
                    Stock = p.Stock,
                });
            model.Count = _productService.CountWithFilters(model.Search);
            return View(model);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorization(UserRole.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateProductModel form)
        {
            if (ModelState.IsValid)
            {
                _productService.Add(new ProductBO(form.Name, form.Description, form.Price, form.Stock));
                TempData.Success("Ajout OK");
                return RedirectToAction("Index");
            }
            return View(form);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Edit(string id)
        {
            try
            {
                ProductBO product = _productService.FindOne(id);
                TempData.Success("Modification OK");
                return View(new EditProductModel
                {
                    Reference = product.Reference,
                    Name = product.Name,
                    Description = product.Description,
                    Price = product.Price,
                    Stock = product.Stock,
                });
            }
            catch (EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
        }

        [HttpPost]
        [Authorization(UserRole.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, EditProductModel form)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _productService.Edit(new ProductBO(id, form.Name, form.Description, form.Price), form.UpdateStock);
                    TempData.Success("Ajout OK");
                    return RedirectToAction("Index");
                }
                catch (EntityNotFoundException)
                {
                    return new HttpNotFoundResult();
                }
                catch(NegativeStockException ex)
                {
                    form.Stock = ex.ActualStock;
                    ModelState.AddModelError("UpdateStock", "The stock cannot be negative");
                }
            }
            return View(form);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Delete(string id)
        {
            try
            {
                _productService.Delete(id);
                TempData.Success($"Le produit ({id}) a été supprimé");
            }
            catch (EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorization(UserRole.Admin, UserRole.Seller)]
        public ActionResult Search(string search)
        {
            IEnumerable<ProductModel> Results = _productService.FindWithFilters(0, 10, search)
                .Select(p => new ProductModel
                {
                    Reference = p.Reference,
                    Name = p.Name,
                    Price = p.Price,
                    Stock = p.Stock,
                    Label = p.Label,
                });
            return Json(Results, JsonRequestBehavior.AllowGet);
        }
    }
}