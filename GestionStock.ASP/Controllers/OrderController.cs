﻿using GestionStock.ASP.Models.Order;
using GestionStock.ASP.Models.Order.Forms;
using GestionStock.ASP.Utils;
using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace GestionStock.ASP.Controllers
{
    [Authorization(UserRole.Admin, UserRole.Seller)]
    public class OrderController : Controller
    {
        private IOrderService _orderService;
        private ICustomerService _customerService;

        public OrderController(IOrderService orderService, ICustomerService customerService)
        {
            _orderService = orderService;
            _customerService = customerService;
        }

        public ActionResult Index(OrderIndexModel model)
        {

            model.Results = _orderService
                .FindWithFilters(model.Offset, model.Limit, model.Reference,model.CustomerRef, model.SelectedStatuses,model.FromDate,model.ToDate)
                .Select(o => new OrderModel {
                        Reference = o.Reference,
                        CustomerRef = o.CustomerRef,
                        Date = o.Date,
                        Status = o.Status,
                        Total = o.Total,
                    }
                );
            model.AllCustomers = GetSelectFromCustomers(true);
            model.AllStatuses = SelectUtils.GetSelectFromEnum<OrderStatus>();
            return View(model);
        }

        public ActionResult Create()
        {
            CreateOrderModel form = new CreateOrderModel
            {
                AllCustomers = GetSelectFromCustomers()
            };
            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateOrderModel form)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    string reference = _orderService.Add(new OrderBO(form.CustomerRef));
                    TempData.Info("Continuer la commande");
                    return RedirectToAction("Edit", new { id = reference });
                }
                catch(EntityNotFoundException ex)
                {
                    ModelState.AddModelError(ex.Property, ex.Message);
                }
            }
            form.AllCustomers = GetSelectFromCustomers();
            return View(form);
        }

        public ActionResult Edit(string id)
        {
            try
            {
                OrderBO order = _orderService.FindOneWithOrderLines(id);
                EditOrderModel model = new EditOrderModel
                {
                    Reference = order.Reference,
                    CustomerRef = order.CustomerRef,
                    Status = order.Status,
                    Date = order.Date,
                    Total = order.Total,
                    OrderLines = order.OrderLines.Select(ol => new OrderLineModel
                    {
                        Id = ol.Id,
                        ProductRef = ol.ProductRef,
                        Quantity = ol.Quantity,
                        UnitPrice = ol.UnitPrice,
                        LinePrice = ol.LinePrice,
                    }).ToList(),
                };
                return View(model);
            }
            catch (EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
        }
        public ActionResult Pend(string id)
        {
            return ChangeStatus(id, OrderStatus.Pending);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Close(string id)
        {
            return ChangeStatus(id, OrderStatus.Closed);
        }

        [Authorization(UserRole.Admin)]
        public ActionResult Cancel(string id)
        {
            return ChangeStatus(id, OrderStatus.Canceled);
        }

        [HttpPost] // cannot enable put method // ask michael or thierry
        public ActionResult Edit(string id, AddOrderLineModel form) 
        {
            if(ModelState.IsValid) // cannot validate model in ajax // ask michael or thierry
            {
                try
                {
                    IEnumerable<OrderLineModel> lines = _orderService.UpdateLines(id, form.ProductRef, form.Quantity)
                        .Select(ol => new OrderLineModel
                        {
                            Id = ol.Id,
                            OrderRef = ol.OrderRef,
                            ProductRef = ol.ProductRef,
                            Quantity = ol.Quantity,
                            UnitPrice = ol.UnitPrice,
                            LinePrice = ol.LinePrice,
                        });
                    // if not called in ajax
                    // no need to map lines and return Redirect or View instead
                    // TempData["success"] = "Enregistrement OK";
                    // return RedirectToAction("Edit", new { id = reference });
                    return Json(new { lines });
                }
                catch (EntityNotFoundException ex)
                {
                    if(ex.Property == null)
                    {
                        return new HttpNotFoundResult();
                    }
                    else
                    {
                        // ModelState.AddModelError(ex.Property, ex.Message);
                        return new HttpStatusCodeResult(400, "Le produit demandé est introuvable");
                    }
                }
                catch(InvalidOperationException)
                {
                    // TempData["error"] = "Vous ne pouvez pas modifier cette commande";
                    return new HttpStatusCodeResult(400, "Vous ne pouvez pas modifier cette commande");
                }
            }
            // return View(model);
            return new HttpStatusCodeResult(400, "Les informations fournies ne sont pas valides");
        }

        private IEnumerable<SelectListItem> GetSelectFromCustomers(bool emptyLine = false)
        {
            IEnumerable<SelectListItem> lines = _customerService.FindAllActive().Select(c => new SelectListItem
            {
                Value = c.Reference,
                Text = c.Label,
            });
            return emptyLine ? SelectUtils.GetOneEmptyLine().Concat(lines) : lines;
        }

        private ActionResult ChangeStatus(string id, OrderStatus status)
        {
            try
            {
                _orderService.ChangeStatus(id, status);
                TempData.Success("Operation OK");
                return RedirectToAction("Index");
            }
            catch (EntityNotFoundException)
            {
                return new HttpNotFoundResult();
            }
            catch (InvalidStatusException ex)
            {
                TempData.Error(ex.Message);
            }
            catch(EmptyOrderException)
            {
                TempData.Error($"Votre commande ne peut pas être vide");
            }
            catch (NegativeStockException ex)
            {
                TempData.Error($"Vous dépassez la limite des stocks du produit ({ex.ProductRef}) max: {ex.ActualStock}");
            }
            catch (SmtpException)
            {
                TempData.Error($"Nous n'avons pas pu envoyer les emails veuillez revérifier l'email du client");
            }
            return RedirectToAction("Edit", new { id });
        }
    }
}