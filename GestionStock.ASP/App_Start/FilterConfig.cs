﻿using GestionStock.ASP.Utils;
using GestionStock.DAL.Abstractions.Enums;
using System.Configuration;
using System.Web.Mvc;

namespace GestionStock.ASP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            // for debug only
            filters.Add(new HandleAuthorizationForDebugFilter());
        }
    }

    class HandleAuthorizationForDebugFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (ConfigurationManager.AppSettings.Get("ENV") == "DEBUG" && !filterContext.HttpContext.Session.IsLogged())
            {
                filterContext.HttpContext.Session.Start(1, "lykhun@gmail.com", UserRole.Admin);
            }
        }
    }
}
