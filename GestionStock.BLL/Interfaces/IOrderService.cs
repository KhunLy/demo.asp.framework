﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;

namespace GestionStock.BLL.Services
{
    public interface IOrderService
    {
        string Add(OrderBO order);
        void ChangeStatus(string reference, OrderStatus newStatus);
        OrderBO FindOneWithOrderLines(string reference);
        IEnumerable<OrderBO> FindWithFilters(int offset, int limit, string reference, string customerRef, List<OrderStatus> orderStatuses, DateTime? fromDate, DateTime? toDate);
        IEnumerable<OrderStatusRepartitionBO> GetCountAndSumGroupByStatuses();
        IEnumerable<OrderLineBO> UpdateLines(string reference, string productRef, int quantity);
    }
}