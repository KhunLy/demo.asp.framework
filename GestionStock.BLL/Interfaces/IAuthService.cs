﻿using GestionStock.BLL.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.BLL.Services
{
    public interface IAuthService
    {

        UserBO Login(string email, string password);
        bool ResetPassword(string token, string password);
        void AskResetPassword(string email);
    }
}
