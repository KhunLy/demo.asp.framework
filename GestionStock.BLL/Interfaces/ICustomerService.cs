﻿using GestionStock.BLL.BusinessObjects;
using System.Collections.Generic;

namespace GestionStock.BLL.Services
{
    public interface ICustomerService
    {
        string Add(CustomerBO newCustomer);
        int CountWithFilters(string search);
        void Delete(string reference);
        void Edit(CustomerBO newCustomer);
        IEnumerable<CustomerBO> FindAllActive();
        CustomerBO FindOne(string reference);
        IEnumerable<CustomerBO> FindWithFilters(string search, int offset, int limit);
    }
}