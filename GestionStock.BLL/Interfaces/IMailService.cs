﻿using System.Collections.Generic;
using System.Net.Mail;

namespace GestionStock.BLL.Services
{
    public interface IMailService
    {
        void SendEmail(string subject, string body, IEnumerable<Attachment> attachments, params string[] to);
        void SendEmail(string subject, string body, params string[] to);
    }
}