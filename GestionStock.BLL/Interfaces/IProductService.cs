﻿using GestionStock.BLL.BusinessObjects;
using System.Collections.Generic;

namespace GestionStock.BLL.Services
{
    public interface IProductService
    {
        string Add(ProductBO newProduct);
        int CountWithFilters(string search);
        void Delete(string reference);
        void Edit(ProductBO product, int updateStock);
        ProductBO FindOne(string reference);
        IEnumerable<ProductBO> FindWithFilters(int offset, int limit, string search);
        IEnumerable<ProductBO> FindOrderByQuantity(int v);
    }
}