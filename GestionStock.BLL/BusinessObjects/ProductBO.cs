﻿using GestionStock.DAL.Abstractions.Entities;

namespace GestionStock.BLL.BusinessObjects
{
    public class ProductBO
    {
        public ProductBO(string reference, string name, string description, decimal price)
        {
            Reference = reference;
            Name = name;
            Description = description;
            Price = price;
        }

        public ProductBO(string name, string description, decimal price, int stock)
        {
            Name = name;
            Description = description;
            Price = price;
            Stock = stock;
        }

        public ProductBO(Product product)
            : this(product.Name, product.Description, product.Price, product.Stock)
        {
            Reference = product.Reference;
        }

        public string Reference { get; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Label
        {
            get => $"{Name} ({Description}) stock: {Stock} prix: {Price}";
        }
        public int Stock { get; set; }
    }
}
