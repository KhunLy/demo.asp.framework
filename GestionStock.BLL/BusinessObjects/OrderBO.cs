﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GestionStock.BLL.BusinessObjects
{
    public class OrderBO
    {
        public OrderBO(string customerRef)
        {
            CustomerRef = customerRef;
        }

        public OrderBO(Order order)
        {
            Reference = order.Reference;
            Date = order.Date;
            Status = order.Status;
            CustomerRef = order.CustomerRef;
            if(order.OrderLines != null)
            {
                OrderLines = order.OrderLines.Select(ol => new OrderLineBO(ol));
            }
            if(order.Customer != null)
            {
                Customer = new CustomerBO(order.Customer);
            }
        }
        public string Reference { get; }
        public string CustomerRef { get; set; }
        public DateTime Date { get; }
        public OrderStatus Status { get; set; }

        public decimal Total
        {
            get
            {
                return OrderLines?.Sum(ol => ol.LinePrice) ?? 0;
            }
        }
        public IEnumerable<OrderLineBO> OrderLines { get; set; }
        public CustomerBO Customer { get; set; }
    }
}
