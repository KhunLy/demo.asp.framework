﻿
using GestionStock.DAL.Abstractions.Enums;
using GestionStock.DAL.Abstractions.Views;

namespace GestionStock.BLL.BusinessObjects
{
    public class OrderStatusRepartitionBO
    {

        public OrderStatusRepartitionBO(OrderStatusRepartition orderStatusRepartition)
        {
            SubTotal = orderStatusRepartition.SubTotal;
            Count = orderStatusRepartition.Count;
            Status = orderStatusRepartition.Status;
        }

        public decimal SubTotal { get; }
        public int Count { get; }
        public OrderStatus Status { get; }
    }
}
