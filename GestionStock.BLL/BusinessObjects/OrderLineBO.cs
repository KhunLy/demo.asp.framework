﻿using GestionStock.DAL.Abstractions.Entities;

namespace GestionStock.BLL.BusinessObjects
{
    public class OrderLineBO
    {
        public OrderLineBO(OrderLine ol)
        {
            OrderRef = ol.OrderRef;
            ProductRef = ol.ProductRef;
            Quantity = ol.Quantity;
            if(ol.Product != null)
            {
                UnitPrice = ol.UnitPrice ?? ol.Product.Price;
                Product = new ProductBO(ol.Product);
            }
        }
        public int Id { get; }
        public string OrderRef { get; }
        public string ProductRef { get; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public ProductBO Product { get; set; }
        public decimal LinePrice
        {
            get
            {
                return UnitPrice * Quantity;
            }
        }
    }
}
