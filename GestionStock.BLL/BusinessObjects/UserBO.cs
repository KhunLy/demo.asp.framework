﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using System;

namespace GestionStock.BLL.BusinessObjects
{
    public class UserBO
    {

        public UserBO(User user)
        {
            Id = user.Id;
            Email = user.Email;
            Role = user.Role;
            Salt = user.Salt;
            EncodedPassword = user.EncodedPassword;
        }

        public UserBO(int id, string email, UserRole role)
        {
            Id = id;
            Email = email;
            Role = role;
        }

        public int Id { get; }
        public string Email { get; }
        public UserRole Role { get; }
        public Guid Salt { get; }
        public byte[] EncodedPassword { get; }
        public string Password { get; }
    }
}
