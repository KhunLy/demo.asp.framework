﻿using GestionStock.DAL.Abstractions.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GestionStock.BLL.BusinessObjects
{
    public class CustomerBO
    {
        public CustomerBO(string reference, string email, string phone)
        {
            Reference = reference;
            Email = email;
            Phone = phone;
        }

        public CustomerBO(string lastName, string firstName, string email, string phone)
        {
            LastName = lastName;
            FirstName = firstName;
            Email = email;
            Phone = phone;
        }

        public CustomerBO(Customer entity)
            : this(entity.LastName, entity.FirstName, entity.Email, entity.Phone)
        {
            Reference = entity.Reference;
            Deleted = entity.Deleted;
            if(entity.Orders != null)
            {
                Orders = entity.Orders
                    .Select(o => new OrderBO(o));
            }
        }
        public string Reference { get; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool Deleted { get; }
        public string Label
        {
            get => $"{LastName} {FirstName}({Reference})";
        }
        public IEnumerable<OrderBO> Orders { get; set; }
    }
}
