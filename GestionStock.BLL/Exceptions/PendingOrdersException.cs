﻿using System;

namespace GestionStock.BLL.Exceptions
{
    public class PendingOrdersException : Exception
    {
        public PendingOrdersException()
            : base()
        {

        }
    }
}
