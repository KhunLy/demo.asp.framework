﻿using System;

namespace GestionStock.BLL.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public string Property { get; }

        public EntityNotFoundException(string property)
            : base("Entity cannot be found")
        {
            Property = property;
        }
        public EntityNotFoundException()
            : base("Entity cannot be found")
        {

        }
    }
}
