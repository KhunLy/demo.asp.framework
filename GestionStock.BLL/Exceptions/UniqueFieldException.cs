﻿using System;

namespace GestionStock.BLL.Exceptions
{
    public class UniqueFieldException : Exception
    {
        public string FieldName { get; }
        public object Value { get; }

        public UniqueFieldException(string fieldName, object value)
            : base($"{ fieldName } already exists with value { value }")
        {
            FieldName = fieldName;
            Value = value;
        }
    }
}
