﻿using GestionStock.DAL.Abstractions.Enums;
using System;

namespace GestionStock.BLL.Exceptions
{
    public class InvalidStatusException : Exception
    {
        public InvalidStatusException(OrderStatus previous, OrderStatus next)
            :base($"The order status cannot be update from {previous} to {next}")
        {

        }
    }
}
