﻿using System;

namespace GestionStock.BLL.Exceptions
{
    public class NegativeStockException : Exception
    {
        public int ActualStock { get; }
        public string ProductRef { get; }
        public NegativeStockException(string productRef, int actualStock)
            :base("Stock cannot be negative")
        {
            ActualStock = actualStock;
            ProductRef = productRef;
        }

        public NegativeStockException(int actualStock)
            : base("Stock cannot be negative")
        {
            ActualStock = actualStock;
        }
    }
}
