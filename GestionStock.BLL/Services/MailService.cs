﻿using GestionStock.BLL.Configs;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace GestionStock.BLL.Services
{
    public class MailService : IMailService
    {
        private readonly MailConfig _config;
        private readonly SmtpClient _client;

        public MailService(MailConfig config, SmtpClient client)
        {
            _config = config;
            _client = client;
            _client.Host = _config.Host;
            _client.Port = _config.Port;
            _client.Credentials = new NetworkCredential(_config.Email, _config.Password);
            _client.EnableSsl = true;
        }

        public void SendEmail(string subject, string body, params string[] to)
        {
            SendEmail(subject, body, null, to);
        }

        public void SendEmail(string subject, string body, IEnumerable<Attachment> attachments, params string[] to)
        {
            // to prevent email sending
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(_config.Email);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                if (attachments != null)
                {
                    foreach (Attachment attachment in attachments)
                    {
                        mailMessage.Attachments.Add(attachment);
                    }
                }
                if(_config.IsDebug)
                {
                    // send email to sender on debug
                    mailMessage.To.Add(new MailAddress(_config.Email));
                }
                else
                {
                    foreach (string dest in to)
                    {
                        mailMessage.To.Add(new MailAddress(dest));
                    }
                }
                _client.Send(mailMessage);
            }
        }
    }
}
