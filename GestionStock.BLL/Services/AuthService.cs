﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Interfaces;
using System;
using Toolbox.Security.Interfaces;

namespace GestionStock.BLL.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly IHashService _hashService;
        private readonly IJwtService _jwtService;
        private readonly IMailService _mailService;

        public AuthService(IUserRepository userRepository, IHashService hashService, IJwtService jwtService, IMailService mailService)
        {
            _userRepository = userRepository;
            _hashService = hashService;
            _jwtService = jwtService;
            _mailService = mailService;
        }

        public void AskResetPassword(string email)
        {
            throw new NotImplementedException();
        }

        public UserBO Login(string email, string password)
        {
            User user = _userRepository.FindOneByEmail(email);
            if (user == null)
            {
                throw new InvalidCredentialsException();
            }
  
            if (!_hashService.IsValid(password + user.Salt.ToString(), user.EncodedPassword))
            {
                throw new InvalidCredentialsException();
            }
            return new UserBO(user);
        }

        public bool ResetPassword(string token, string password)
        {
            throw new NotImplementedException();
        }
    }
}
