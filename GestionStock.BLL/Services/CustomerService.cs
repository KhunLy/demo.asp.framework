﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using GestionStock.DAL.Abstractions.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace GestionStock.BLL.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;

        public CustomerService(ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
        }

        public IEnumerable<CustomerBO> FindWithFilters(string search, int offset, int limit)
        {
            return _customerRepository.FindActiveOrderByReferenceWithFilters(search, offset, limit)
                .Select(c => new CustomerBO(c));
        }

        public IEnumerable<CustomerBO> FindAllActive()
        {
            return _customerRepository.FindAllActiveOrderByReference()
                .Select(c => new CustomerBO(c));
        }

        public int CountWithFilters(string search)
        {
            return _customerRepository.CountActiveWithFilters(search);
        }

        public CustomerBO FindOne(string reference)
        {
            Customer entity = _customerRepository.FindOneActive(reference);
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
            return new CustomerBO(entity);
        }

        public string Add(CustomerBO newCustomer)
        {
            string newRef = CreateRef(newCustomer.LastName, newCustomer.FirstName);
            if (_customerRepository.Exists(c => c.Email == newCustomer.Email))
            {
                throw new UniqueFieldException(nameof(newCustomer.Email), newCustomer.Email);
            }
            Customer toAdd = new Customer
            {
                Reference = newRef,
                LastName = newCustomer.LastName,
                FirstName = newCustomer.FirstName,
                Email = newCustomer.Email,
                Phone = newCustomer.Phone,
                Deleted = false
            };
            _customerRepository.Add(toAdd);
            return newRef;
        }

        public void Edit(CustomerBO newCustomer)
        {
            Customer toUpdate = _customerRepository.FindOneActive(newCustomer.Reference);
            if (toUpdate == null)
            {
                throw new EntityNotFoundException();
            }
            if (_customerRepository.Exists(c => c.Email == newCustomer.Email && c.Reference != newCustomer.Reference))
            {
                throw new UniqueFieldException(nameof(newCustomer.Email), newCustomer.Email);
            }
            toUpdate.Email = newCustomer.Email;
            toUpdate.Phone = newCustomer.Phone;
            _customerRepository.Update(toUpdate);
        }

        public void Delete(string reference)
        {
            Customer entity = _customerRepository.FindOneActive(reference);
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
            // cannot delete Customer if he has pending orders
            if (_orderRepository.Exists(o => o.CustomerRef == reference && o.Status == OrderStatus.Pending))
            {
                throw new PendingOrdersException();
            }

            using (TransactionScope scope = new TransactionScope())
            {
                // need to cancel all Customer's "InProgress" orders
                _orderRepository.FindWhere(o => o.CustomerRef == reference && o.Status == OrderStatus.InProgress)
                    .ToList().ForEach(o =>
                    {
                        o.Status = OrderStatus.Canceled;
                        _orderRepository.Update(o);
                    });
                entity.Deleted = true;
                _customerRepository.Update(entity);
                scope.Complete();
            }
        }

        private string CreateRef(string lastName, string firstName)
        {
            string firstLetters = (lastName.Substring(0, 2) + firstName.Substring(0, 2)).ToUpper();
            int count = _customerRepository.CountWhere(c => c.Reference.StartsWith(firstLetters));
            string number = $"{count + 1}".PadLeft(4, '0');
            return $"{firstLetters}{number}";
        }
    }
}
