﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using GestionStock.DAL.Abstractions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace GestionStock.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMailService _mailService;

        public OrderService(IOrderRepository orderRepository, IProductRepository productRepository, ICustomerRepository customerRepository, IMailService mailService)
        {
            _orderRepository = orderRepository;
            _productRepository = productRepository;
            _customerRepository = customerRepository;
            _mailService = mailService;
        }

        public IEnumerable<OrderBO> FindWithFilters(int offset, int limit, string reference, string customerRef, List<OrderStatus> orderStatuses, DateTime? fromDate, DateTime? toDate)
        {
            return _orderRepository.FindOrderByDateWithFilters(offset, limit, reference, customerRef, orderStatuses, fromDate, toDate)
                .Select(o => new OrderBO(o));
        }

        public string Add(OrderBO order)
        {
            DateTime date = DateTime.Now;
            string newRef = CreateRef(date);

            Customer customer = _customerRepository.FindOneActive(order.CustomerRef);
            if (customer is null)
            {
                throw new EntityNotFoundException(nameof(order.CustomerRef));
            }

            Order newOrder = new Order
            {
                Reference = newRef,
                CustomerRef = order.CustomerRef,
                Customer = customer,
                Date = date,
                Status = OrderStatus.InProgress,
            };
            _orderRepository.Add(newOrder);
            return newRef;
        }

        private string CreateRef(DateTime date)
        {
            string result = date.ToString("yyMMdd");
            int count = _orderRepository.CountWhere(x => x.Reference.StartsWith(result));
            string number = $"{count + 1}".PadLeft(4, '0');
            result += number;
            return result;
        }

        public OrderBO FindOneWithOrderLines(string reference)
        {
            Order o = _orderRepository.FindOneWithOrderLines(reference);
            if (o is null)
            {
                throw new EntityNotFoundException();
            }
            return new OrderBO(o);
        }

        public IEnumerable<OrderLineBO> UpdateLines(string reference, string productRef, int quantity)
        {
            Order o = _orderRepository.FindOneWithOrderLines(reference);
            if (o is null)
            {
                throw new EntityNotFoundException();
            }
            if (o.Status != OrderStatus.InProgress)
            {
                throw new InvalidOperationException();
            }
            Product product = _productRepository.FindOneActive(productRef);
            if (product is null)
            {
                throw new EntityNotFoundException(nameof(productRef));
            }
            OrderLine line = o.OrderLines.FirstOrDefault(ol => ol.ProductRef == productRef);
            if (line != null)
            {
                // si la ligne existe déjà on modifie la quantité ou on retire la ligne
                if (quantity == 0)
                {
                    _orderRepository.RemoveLine(line);
                }
                else
                {
                    _orderRepository.UpdateLine(line, quantity);
                }
            }
            else
            {
                if (quantity != 0) // pas besoin d'ajouter une ligne si la qty == 0
                {
                    // sinon on crée une nouvelle ligne
                    line = new OrderLine { OrderRef = reference, ProductRef = productRef, Quantity = quantity };
                    _orderRepository.AddLine(line);
                }
            }
            // mise à jour de la date de modification
            o.Date = DateTime.Now;
            _orderRepository.Update(o);
            return o.OrderLines.Select(ol => new OrderLineBO(ol));
        }

        public void ChangeStatus(string reference, OrderStatus newStatus)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Order order = _orderRepository.FindOneWithOrderLines(reference);
                if (order == null)
                {
                    throw new EntityNotFoundException();
                }

                CheckStatus(order, newStatus);

                if (order.Status == OrderStatus.InProgress && newStatus != OrderStatus.Canceled)
                {
                    PendOrder(order);
                }

                if (newStatus == OrderStatus.Closed)
                {
                    CloseOrder(order);
                }

                if (newStatus == OrderStatus.Canceled && order.Status == OrderStatus.Pending)
                {
                    CancelOrder(order);
                }

                order.Status = newStatus;
                _orderRepository.Update(order);
                scope.Complete();
            }
        }

        private void CloseOrder(Order order)
        {
            _mailService.SendEmail($"Commande {order.Reference} payée", $"Nous avons bien reçu le paiement pour la commande {order.Reference}", order.Customer.Email);
        }

        private void CancelOrder(Order order)
        {
            // fill the product stock back
            foreach (OrderLine item in order.OrderLines)
            {
                item.Product.Stock += item.Quantity;
            }
            _mailService.SendEmail($"Commande {order.Reference} annulée", $"Votre commande {order.Reference} a été annulée", order.Customer.Email);
        }

        private void PendOrder(Order order)
        {
            if (order.OrderLines.Count == 0)
            {
                throw new EmptyOrderException();
            }
            foreach (OrderLine item in order.OrderLines)
            {
                // update all order Lines with current unitPrice
                item.UnitPrice = item.Product.Price;
                // check all productStock
                if (item.Product.Stock - item.Quantity < 0)
                {
                    throw new NegativeStockException(item.ProductRef, item.Product.Stock);
                }
                // update all productStock
                item.Product.Stock -= item.Quantity;
            }
            _mailService.SendEmail($"Commande {order.Reference} confirmée", $"Votre commande {order.Reference} est confirmée", order.Customer.Email);
        }

        private void CheckStatus(Order order, OrderStatus newStatus)
        {
            // validate the changing status
            if (order.Status == OrderStatus.Closed || order.Status == OrderStatus.Canceled)
            {
                throw new InvalidStatusException(order.Status, newStatus);
            }
            if ((int)order.Status >= (int)newStatus) // cannot downgrade to previous status
            {
                throw new InvalidStatusException(order.Status, newStatus);
            }
        }

        public IEnumerable<OrderStatusRepartitionBO> GetCountAndSumGroupByStatuses()
        {
            return _orderRepository.GetCountAndSumGroupByStatuses().Select(os => new OrderStatusRepartitionBO(os));
        }
    }
}
