﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GestionStock.BLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public IEnumerable<ProductBO> FindWithFilters(int offset, int limit, string search)
        {
            return _productRepository.FindActiveOrderByReferenceWithFilters(offset, limit, search)
                .Select(p => new ProductBO(p));
        }

        public int CountWithFilters(string search)
        {
            return _productRepository.CountActiveWithFilters(search);
        }

        public ProductBO FindOne(string reference)
        {
            Product entity = _productRepository.FindOneActive(reference);
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
            return new ProductBO(entity);
        }

        public string Add(ProductBO newProduct)
        {
            string newRef = CreateRef(newProduct.Name);
            Product toAdd = new Product()
            {
                Reference = newRef,
                Name = newProduct.Name,
                Description = newProduct.Description,
                Price = newProduct.Price,
                Stock = newProduct.Stock,
                Deleted = false
            };
            _productRepository.Add(toAdd);
            return newRef;
        }

        public void Edit(ProductBO product, int updateStock)
        {
            Product toUpdate = _productRepository.FindOneActive(product.Reference);
            if (toUpdate is null)
            {
                throw new EntityNotFoundException();
            }
            if (toUpdate.Stock + updateStock < 0)
            {
                throw new NegativeStockException(toUpdate.Stock);
            }
            toUpdate.Name = product.Name;
            toUpdate.Description = product.Description;
            toUpdate.Price = product.Price;
            toUpdate.Stock += updateStock;
            _productRepository.Update(toUpdate);
        }

        public void Delete(string reference)
        {
            Product toDelete = _productRepository.FindOneActive(reference);
            if (toDelete is null)
            {
                throw new EntityNotFoundException();
            }
            toDelete.Deleted = true;
            _productRepository.Update(toDelete);
        }

        private string CreateRef(string name)
        {
            string firstLetters = name.Substring(0, 4).ToUpper();
            int count = _productRepository.CountWhere(c => c.Reference.StartsWith(firstLetters));
            string number = $"{count + 1}".PadLeft(4, '0');
            return $"{firstLetters}{number}".ToUpper();
        }

        public IEnumerable<ProductBO> FindOrderByQuantity(int limit)
        {
            return _productRepository.FindOrderByQuantity(limit).Select(p => new ProductBO(p));
        }
    }
}
