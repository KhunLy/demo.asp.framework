﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using GestionStock.DAL.Abstractions.Enums;
using System.Linq;

namespace GestionStock.BLL.Mocks
{
    class Credentials 
    {
        public Credentials(int id, string email, string password, UserRole role, string token)
        {
            Id = id;
            Email = email;
            Password = password;
            Role = role;
            Token = token;
        }

        public int Id { get; set; } 
        public string Email { get; set; } 
        public string Password { get; set; }
        public UserRole Role { get; set; }
        public string Token { get; set; } 
    }
    public class FakeAuthService : IAuthService
    {
        private static Credentials[] _validCredentials = { 
            new Credentials(1, "lykhun@gmail.com", "Test1234=", UserRole.Admin, "FAKE_TOKEN_1"),
            new Credentials(2, "admin@yopmail.be", "Test1234=", UserRole.Seller, "FAKE_TOKEN_2"),
        };
        public UserBO Login(string email, string password)
        {
            Credentials cred = _validCredentials.FirstOrDefault(c => c.Email.ToLower() == email.ToLower());
            if(cred == null || cred.Password != password)
            {
                throw new InvalidCredentialsException();
            }
            return new UserBO(cred.Id, cred.Email, cred.Role);
        }

        public void AskResetPassword(string email)
        {
            Credentials cred = _validCredentials.FirstOrDefault(c => c.Email == email.ToLower());
            if(cred == null)
            {
                throw new EntityNotFoundException();
            }
            // create token
            // send email
            
        }

        public bool ResetPassword(string token, string password)
        {
            Credentials user = _validCredentials.FirstOrDefault(c => c.Token == token);
            if(user == null)
            {
                throw new EntityNotFoundException();
            }
            if(password == user.Email)
            {
                throw new MustChangePasswordException();
            }
            user.Password = password;
            return true;
        }
    }
}
