﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Toolbox.Security.Interfaces;

namespace Toolbox.Security.Services
{
    public class HashService : IHashService
    {
        private readonly HashAlgorithm algorithm;

        public HashService(HashAlgorithm algorithm)
        {
            this.algorithm = algorithm;
        }

        public byte[] Hash(string value)
        {
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
        }

        public bool IsValid(string value, byte[] encoded)
        {
            return Hash(value).SequenceEqual(encoded);
        }
    }
}
