﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Toolbox.Security.Interfaces;

namespace Toolbox.Security.Services
{
    public class JwtService : IJwtService
    {
        private readonly JwtSecurityTokenHandler _handler;
        private readonly JwtConfig _config;

        public JwtService(JwtSecurityTokenHandler handler, JwtConfig config)
        {
            if(config == null || config.Signature == null)
            {
                throw new ArgumentNullException(nameof(config));
            }
            _handler = handler;
            _config = config;
        }

        public string CreateToken(params Claim[] claims)
        {
            return CreateToken(claims);
        }

        public string CreateToken(IEnumerable<Claim> claims)
        {
            JwtSecurityToken token = new JwtSecurityToken(_config.Issuer, _config.Audience, claims, DateTime.Now, DateTime.Now.AddSeconds(_config.Duration), new SigningCredentials(CreateSigningKey(), SecurityAlgorithms.HmacSha512));
            return _handler.WriteToken(token);
        }

        public ClaimsPrincipal ValidateToken(string token)
        {
            try
            {
                return _handler.ValidateToken(token, GetValidationParameters(), out SecurityToken secutityToken);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters
            {
                IssuerSigningKey = CreateSigningKey(),
                ValidAudience = _config.Audience,
                ValidIssuer = _config.Issuer,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = _config.ValidateLifeTime,
                ValidateAudience = _config.ValidateAudience,
                ValidateIssuer = _config.ValidateIssuer,
            };
        }

        private SecurityKey CreateSigningKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Signature));
        }
    }

    public class JwtConfig
    {
        public string Signature { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Duration { get; set; } // seconds

        public bool ValidateIssuer { get; set; }
        public bool ValidateAudience { get; set; }
        public bool ValidateLifeTime { get; set; }
    }
}
