﻿namespace Toolbox.Security.Interfaces
{
    public interface IHashService
    {
        byte[] Hash(string value);
        bool IsValid(string value, byte[] encoded);
    }
}