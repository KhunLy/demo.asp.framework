﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Toolbox.Security.Interfaces
{
    public interface IJwtService
    {
        string CreateToken(params Claim[] claims);
        string CreateToken(IEnumerable<Claim> claims);
        ClaimsPrincipal ValidateToken(string token);
    }
}
