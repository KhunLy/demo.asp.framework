﻿namespace GestionStock.DAL.Migrations
{
    using GestionStock.DAL.Abstractions.Entities;
    using GestionStock.DAL.Abstractions.Enums;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using Toolbox.Security.Services;

    internal sealed class Configuration : DbMigrationsConfiguration<GestionStock.DAL.GestionStockContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GestionStockContext context)
        {
            if(context.Users.Count() == 0)
            {
                context.Users.AddRange(CreateUsers().ToList());
            }
            if(context.Products.Count() == 0)
            {
                context.Products.AddRange(CreateProducts().ToList());
            }
            if(context.Customers.Count() == 0)
            {
                context.Customers.AddRange(CreateCustomersWithOrders().ToList());
            }
        }

        private IEnumerable<User> CreateUsers()
        {
            HashService hashService = new HashService(new SHA512CryptoServiceProvider());
            Guid salt = Guid.NewGuid();
            yield return new User { Id = 1, Email = "admin@yopmail.com", Role = UserRole.Admin, Salt = salt, EncodedPassword = hashService.Hash("admin" + salt.ToString()) };
            Guid salt2 = Guid.NewGuid();
            yield return new User { Id = 1, Email = "seller@yopmail.com", Role = UserRole.Seller, Salt = salt2, EncodedPassword = hashService.Hash("seller" + salt2.ToString()) };
            Guid salt3 = Guid.NewGuid();
            yield return new User { Id = 1, Email = "restocker@yopmail.com", Role = UserRole.Seller, Salt = salt3, EncodedPassword = hashService.Hash("restocker" + salt3.ToString()) };
        }
        private IEnumerable<Product> CreateProducts()
        {
            yield return new Product { Reference = "COCA0001", Name = "Coca Cola 33cl", Description = "CAN. 24X33cl", Price = 16.8m, Stock = 1000 };
            yield return new Product { Reference = "COCA0002", Name = "Coca Cola 50cl", Description = "CAN. 24X50cl", Price = 19.92m, Stock = 1000 };
            yield return new Product { Reference = "COCA0003", Name = "Coca Cola 1l", Description = "BOUT. 6X1l", Price = 10.92m, Stock = 1000 };
            yield return new Product { Reference = "FANT0001", Name = "Fanta Orange 33cl", Description = "CAN. 24X33cl", Price = 16.8m, Stock = 1000 };
            yield return new Product { Reference = "FANT0002", Name = "Fanta Citron 33cl", Description = "CAN. 24X33cl", Price = 16.8m, Stock = 1000 };
            yield return new Product { Reference = "FANT0003", Name = "Fanta Orange 50cl", Description = "CAN. 24X50cl", Price = 16.8m, Stock = 1000 };
            yield return new Product { Reference = "FANT0004", Name = "Fanta Citron 50cl", Description = "CAN. 24X50cl", Price = 19.92m, Stock = 1000 };
            yield return new Product { Reference = "JUPI0001", Name = "Jupiler 33cl", Description = "CAN. 24X33cl", Price = 29.04m, Stock = 1000 };
            yield return new Product { Reference = "JUPI0002", Name = "Jupiler 50cl", Description = "CAN. 24X50cl", Price = 35.52m, Stock = 1000 };
            yield return new Product { Reference = "CARL0001", Name = "Carlsberg 33cl", Description = "CAN. 24X33cl", Price = 32.4m, Stock = 1000 };
            yield return new Product { Reference = "CHIM0001", Name = "Chimay Bleue 33cl", Description = "BOUT. 24X33cl", Price = 45.6m, Stock = 1000 };
            yield return new Product { Reference = "CHIM0002", Name = "Chimay Rouge 33cl", Description = "BOUT. 24X33cl", Price = 45.6m, Stock = 1000 };
            yield return new Product { Reference = "CHIM0003", Name = "Chimay Blanche 33cl", Description = "BOUT. 24X33cl", Price = 30.24m, Stock = 1000 };
            yield return new Product { Reference = "NALU0001", Name = "Nalu Vert 25cl", Description = "CAN. 24X25cl", Price = 16.8m, Stock = 1000 };
            yield return new Product { Reference = "EVIA0001", Name = "Evian 1l", Description = "BOUT. 8X1l", Price = 8.96m, Stock = 1000 };
            yield return new Product { Reference = "EVIA0002", Name = "Evian 50cl", Description = "BOUT. 24X50cl", Price = 5.6m, Stock = 1000 };
            yield return new Product { Reference = "VITT0001", Name = "Vittel 1l", Description = "BOUT. 8X1l", Price = 8.72m, Stock = 1000 };
            yield return new Product { Reference = "VITT0002", Name = "Vittel 50cl", Description = "BOUT. 24X50cl", Price = 5.44m, Stock = 1000 };
            yield return new Product { Reference = "OASI0001", Name = "Oasis Orange 2l", Description = "BOUT. 6X2l", Price = 13.44m, Stock = 1000 };
            yield return new Product { Reference = "OASI0002", Name = "Oasis Tropical 2l", Description = "BOUT. 6X2l", Price = 13.44m, Stock = 1000 };

        }
        private IEnumerable<Customer> CreateCustomersWithOrders()
        {
            yield return new Customer { Reference = "LYKH0001", LastName = "Ly", FirstName = "Khun", Email = "lykhun@gmail.com",
                Orders = new List<Order> {
                new Order { Reference = DateTime.Now.ToString("yyMMdd") + "0001", Date = DateTime.Now, Status = OrderStatus.InProgress, OrderLines = new List<OrderLine> {
                    new OrderLine { Id = 1, Quantity = 3, ProductRef = "COCA0001" }
                } }
            }
            };
            yield return new Customer { Reference = "LYPI0001", LastName = "Ly", FirstName = "Piv", Email = "piv.ly@bstorm.be" };
            yield return new Customer { Reference = "PEMI0001", LastName = "Person", FirstName = "Mike", Email = "michael.person@cognitic.be" };
            yield return new Customer { Reference = "MOTH0001", LastName = "Morre", FirstName = "Thierry", Email = "tierry.morre@cognitic.be" };
            yield return new Customer { Reference = "COJU0001", LastName = "Coppin", FirstName = "Julien", Email = "julien.coppin@bstorm.be" };
            yield return new Customer { Reference = "COJU0002", LastName = "Courtois", FirstName = "Julie", Email = "julie@courtois.be" };
            yield return new Customer { Reference = "STAU0001", LastName = "Strimelle", FirstName = "Aurélien", Email = "aurelien.strimelle@bstorm.be" };
            yield return new Customer { Reference = "OVFL0001", LastName = "Ovyn", FirstName = "Flavian", Email = "flavian.ovyn@bstorm.be" };
            yield return new Customer { Reference = "LAST0001", LastName = "Laurent", FirstName = "Steve", Email = "steve.laurent@bstorm.be" };
            yield return new Customer { Reference = "BALO0001", LastName = "Baudoux", FirstName = "Loïc", Email = "loic.baudoux@bstorm.be" };
            yield return new Customer { Reference = "PEMI0002", LastName = "Pedro", FirstName = "Michel", Email = "michel@pedro.be" };
            yield return new Customer { Reference = "COJU0003", LastName = "Constant", FirstName = "Jules", Email = "jules@constant.be" };
        }
    }
}
