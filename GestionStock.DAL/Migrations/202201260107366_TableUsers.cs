﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        email = c.String(nullable: false, maxLength: 255, unicode: false),
                        role = c.Int(nullable: false),
                        encodedPassword = c.Binary(nullable: false),
                        salt = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.email, unique: true)
                .Index(t => t.salt, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "salt" });
            DropIndex("dbo.Users", new[] { "email" });
            DropTable("dbo.Users");
        }
    }
}
