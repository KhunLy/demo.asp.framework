﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablesCustomersAndOrders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        reference = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                        lastName = c.String(nullable: false, maxLength: 50, unicode: false),
                        firstName = c.String(nullable: false, maxLength: 50, unicode: false),
                        email = c.String(nullable: false, maxLength: 255, unicode: false),
                        phone = c.String(maxLength: 25, unicode: false),
                        deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.reference)
                .Index(t => t.email, unique: true);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        reference = c.String(nullable: false, maxLength: 10, fixedLength: true, unicode: false),
                        date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        status = c.Int(nullable: false),
                        customerRef = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.reference)
                .ForeignKey("dbo.Customers", t => t.customerRef, cascadeDelete: true)
                .Index(t => t.customerRef);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "customerRef", "dbo.Customers");
            DropIndex("dbo.Orders", new[] { "customerRef" });
            DropIndex("dbo.Customers", new[] { "email" });
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
        }
    }
}
