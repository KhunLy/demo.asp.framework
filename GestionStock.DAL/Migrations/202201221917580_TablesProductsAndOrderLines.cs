﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablesProductsAndOrderLines : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        orderRef = c.String(nullable: false, maxLength: 10, fixedLength: true, unicode: false),
                        productRef = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                        quantity = c.Int(nullable: false),
                        unitPrice = c.Decimal(nullable: false, storeType: "money"),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Orders", t => t.orderRef, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productRef, cascadeDelete: true)
                .Index(t => t.orderRef)
                .Index(t => t.productRef);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        reference = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                        name = c.String(nullable: false, maxLength: 100, unicode: false),
                        description = c.String(),
                        stock = c.Int(nullable: false),
                        price = c.Decimal(nullable: false, storeType: "money"),
                        deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.reference);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLines", "productRef", "dbo.Products");
            DropForeignKey("dbo.OrderLines", "orderRef", "dbo.Orders");
            DropIndex("dbo.OrderLines", new[] { "productRef" });
            DropIndex("dbo.OrderLines", new[] { "orderRef" });
            DropTable("dbo.Products");
            DropTable("dbo.OrderLines");
        }
    }
}
