﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderLineUnitPriceNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.OrderLines", "unitPrice", c => c.Decimal(storeType: "money"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OrderLines", "unitPrice", c => c.Decimal(nullable: false, storeType: "money"));
        }
    }
}
