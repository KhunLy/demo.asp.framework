﻿using GestionStock.DAL.Abstractions.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Configurations
{
    internal class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customers");

            HasKey(c => c.Reference);

            Property(c => c.Reference).HasColumnName("reference").IsUnicode(false).HasMaxLength(8).IsFixedLength();
            Property(c => c.LastName).HasColumnName("lastName").IsUnicode(false).IsRequired().HasMaxLength(50);
            Property(c => c.FirstName).HasColumnName("firstName").IsUnicode(false).IsRequired().HasMaxLength(50);
            Property(c => c.Email).HasColumnName("email").IsUnicode(false).IsRequired().HasMaxLength(255);
            Property(c => c.Phone).HasColumnName("phone").IsUnicode(false).HasMaxLength(25);
            Property(c => c.Deleted).HasColumnName("deleted").IsRequired();
            
            HasIndex(c => c.Email).IsUnique();
        }
    }
}
