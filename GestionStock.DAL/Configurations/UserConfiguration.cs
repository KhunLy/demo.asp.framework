﻿using GestionStock.DAL.Abstractions.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GestionStock.DAL.Configurations
{
    internal class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users");

            HasKey(u => u.Id);

            Property(u => u.Id).HasColumnName("id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(u => u.Email).HasColumnName("email").IsRequired().HasMaxLength(255).IsUnicode(false);
            Property(u => u.Role).HasColumnName("role").IsRequired();
            Property(u => u.EncodedPassword).HasColumnName("encodedPassword").IsRequired();
            Property(u => u.Salt).HasColumnName("salt").IsRequired();

            HasIndex(u => u.Email).IsUnique();
            HasIndex(u => u.Salt).IsUnique();
        }
    }
}
