﻿using GestionStock.DAL.Abstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Configurations
{
    internal class OrderLineConfiguration : EntityTypeConfiguration<OrderLine>
    {

        public OrderLineConfiguration()
        {
            ToTable("OrderLines");

            HasKey(ol => ol.Id);

            Property(ol => ol.Id).HasColumnName("id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(ol => ol.OrderRef).HasColumnName("orderRef").IsRequired().IsUnicode(false).HasMaxLength(10).IsFixedLength();
            Property(ol => ol.ProductRef).HasColumnName("productRef").IsRequired().IsUnicode(false).HasMaxLength(8).IsFixedLength();
            Property(ol => ol.Quantity).HasColumnName("quantity").IsRequired();
            Property(ol => ol.UnitPrice).HasColumnName("unitPrice").HasColumnType("MONEY");

            HasRequired(ol => ol.Order).WithMany(o => o.OrderLines).HasForeignKey(o => o.OrderRef);
            HasRequired(ol => ol.Product).WithMany(p => p.OrderLines).HasForeignKey(o => o.ProductRef);
        }

    }
}
