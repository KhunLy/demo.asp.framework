﻿using GestionStock.DAL.Abstractions.Entities;
using System.Data.Entity.ModelConfiguration;

namespace GestionStock.DAL.Configurations
{
    internal class ProductConfiguration : EntityTypeConfiguration<Product>
    {

        public ProductConfiguration()
        {
            ToTable("Products");

            HasKey(p => p.Reference);

            Property(p => p.Reference).HasColumnName("reference").IsFixedLength().IsUnicode(false).HasMaxLength(8);
            Property(p => p.Name).HasColumnName("name").IsUnicode(false).HasMaxLength(100).IsRequired();
            Property(p => p.Description).HasColumnName("description").IsUnicode(true);
            Property(p => p.Stock).HasColumnName("stock").IsRequired();
            Property(p => p.Price).HasColumnName("price").IsRequired().HasColumnType("MONEY");
            Property(p => p.Deleted).HasColumnName("deleted").IsRequired();
        }
    }
}
