﻿using GestionStock.DAL.Abstractions.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Configurations
{
    internal class OrderConfiguration : EntityTypeConfiguration<Order>
    {

        public OrderConfiguration()
        {
            ToTable("Orders");

            HasKey(o => o.Reference);

            Property(o => o.Reference).HasColumnName("reference").IsUnicode(false).HasMaxLength(10).IsFixedLength();
            Property(o => o.Date).HasColumnName("date").IsRequired().HasColumnType("DATETIME2");
            Property(o => o.Status).HasColumnName("status").IsRequired();
            Property(o => o.CustomerRef).HasColumnName("customerRef").IsUnicode(false).IsRequired().HasMaxLength(8).IsFixedLength();

            HasRequired(o => o.Customer).WithMany(c => c.Orders).HasForeignKey(o => o.CustomerRef);
        }
    }
}
