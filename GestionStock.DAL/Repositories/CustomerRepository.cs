﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GestionStock.DAL.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(GestionStockContext dc) : base(dc)
        {

        }
        public IEnumerable<Customer> FindActiveOrderByReferenceWithFilters(string search = null, int offset = 0, int limit = 20)
        {
            return _dc.Customers
                .Where(c => !c.Deleted)
                .Where(c => search == null
                    || c.Reference.ToLower().StartsWith(search.ToLower())
                    || c.LastName.ToLower().StartsWith(search.ToLower())
                    || c.FirstName.ToLower().StartsWith(search.ToLower())
                    || c.Email.ToLower().StartsWith(search.ToLower())
                )
                .OrderBy(c => c.Reference)
                .Skip(offset)
                .Take(limit);
        }

        public int CountActiveWithFilters(string search = null)
        {
            return _dc.Customers
                .Where(c => !c.Deleted)
                .Count(c => search == null
                    || c.Reference.ToLower().StartsWith(search.ToLower())
                    || c.LastName.ToLower().StartsWith(search.ToLower())
                    || c.FirstName.ToLower().StartsWith(search.ToLower())
                    || c.Email.ToLower().StartsWith(search.ToLower())
                );
        }

        public IEnumerable<Customer> FindAllActiveOrderByReference()
        {
            return _dc.Customers
                .Where(c => !c.Deleted)
                .OrderBy(c => c.Reference);
        }

        public Customer FindOneActive(string reference)
        {
            return _dc.Customers.FirstOrDefault(c => !c.Deleted && c.Reference == reference);
        }
    }
}
