﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using GestionStock.DAL.Abstractions.Interfaces;
using GestionStock.DAL.Abstractions.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GestionStock.DAL.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(GestionStockContext dc) : base(dc)
        {
        }

        public IEnumerable<Order> FindOrderByDateWithFilters(int offset, int limit, string reference, string customerRef, List<OrderStatus> orderStatuses, DateTime? fromDate, DateTime? toDate)
        {
            return _dc.Orders
                .Include(o => o.OrderLines.Select(ol => ol.Product))
                .Where(o => reference == null || o.Reference == reference)
                .Where(o => customerRef == null || o.CustomerRef == customerRef)
                .Where(o => fromDate == null || DbFunctions.TruncateTime(o.Date) >= fromDate)
                .Where(o => toDate == null || DbFunctions.TruncateTime(o.Date) <= toDate)
                .Where(o => orderStatuses.Any(s => s == o.Status))
                .OrderByDescending(o => o.Date)
                .Skip(offset)
                .Take(limit);
        }

        public Order FindOneWithOrderLines(string reference)
        {
            return _dc.Orders
                .Include(o => o.Customer)
                .Include(o => o.OrderLines.Select(ol => ol.Product))
                .FirstOrDefault(o => o.Reference == reference);
        }

        public void RemoveLine(OrderLine line)
        {
            _dc.OrderLines.Remove(line);
            _dc.SaveChanges();
        }

        public void AddLine(OrderLine line)
        {
            _dc.OrderLines.Add(line);
            _dc.SaveChanges();
        }

        public void UpdateLine(OrderLine line, int quantity)
        {
            line.Quantity = quantity;
            _dc.SaveChanges();
        }

        public IEnumerable<OrderStatusRepartition> GetCountAndSumGroupByStatuses()
        {
            return _dc.Orders.Include(o => o.OrderLines).Where(o => o.OrderLines.Count() > 0).GroupBy(o => o.Status).Select(g => new OrderStatusRepartition
            {
                Status = g.Key,
                Count = g.Count(),
                SubTotal = g.Sum(o => o.OrderLines.Sum(ol => ol.Quantity * (ol.UnitPrice ?? ol.Product.Price))) 
            });
        }
    }
}