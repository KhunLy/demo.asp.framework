﻿using GestionStock.DAL.Abstractions.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace GestionStock.DAL.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> 
        where TEntity : class, new()
    {
        protected readonly GestionStockContext _dc;

        public RepositoryBase(GestionStockContext dc)
        {
            _dc = dc;
        }

        public virtual IEnumerable<TEntity> Find()
        {
            return _dc.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> FindWhere(Func<TEntity, bool> predicate)
        {
            return _dc.Set<TEntity>().Where(predicate);
        }

        public virtual TEntity FindOne(params object[] key)
        {
            return _dc.Set<TEntity>().Find(key);
        }

        public virtual TEntity FindOneWhere(Func<TEntity, bool> predicate)
        {
            return _dc.Set<TEntity>().FirstOrDefault(predicate);
        }

        public virtual int Count()
        {
            return _dc.Set<TEntity>().Count();
        }

        public virtual int CountWhere(Func<TEntity, bool> predicate)
        {
            return _dc.Set<TEntity>().Count(predicate);
        }

        public virtual bool Exists(Func<TEntity, bool> predicate)
        {
            return _dc.Set<TEntity>().Any(predicate);
        }

        public virtual TEntity Add(TEntity entity)
        {
            TEntity result = _dc.Set<TEntity>().Add(entity);
            _dc.SaveChanges();
            return result;
        }

        public virtual TEntity Update(TEntity entity)
        {
            DbEntityEntry entityEntry = _dc.Entry(entity);
            entityEntry.State = EntityState.Modified;
            _dc.SaveChanges();
            return (TEntity)entityEntry.Entity;
        }

        public virtual TEntity Delete(TEntity entity)
        {
            TEntity result = _dc.Set<TEntity>().Remove(entity);
            _dc.SaveChanges();
            return result;
        }
    }
}
