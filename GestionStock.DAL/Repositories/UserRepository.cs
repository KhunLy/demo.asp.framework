﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Interfaces;
using System.Linq;

namespace GestionStock.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(GestionStockContext dc) : base(dc)
        {
        }

        public void ChangePassword(User u, byte[] encodedPassword)
        {
            u.EncodedPassword = encodedPassword;
            _dc.SaveChanges();
        }

        public User FindOneByEmail(string email)
        {
            return _dc.Users.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
        }
    }
}
