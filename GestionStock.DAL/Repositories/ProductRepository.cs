﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Interfaces;
using System.Collections.Generic;
using System.Linq;


namespace GestionStock.DAL.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(GestionStockContext dc) : base(dc)
        {
        }

        public IEnumerable<Product> FindActiveOrderByReferenceWithFilters(int offset, int limit, string search)
        {
            return _dc.Products.Where(p => !p.Deleted)
                .Where(p => search == null
                    || p.Name.ToLower().StartsWith(search.ToLower())
                    || p.Reference.ToLower().StartsWith(search.ToLower())
                 )
                .OrderBy(p => p.Reference)
                .Skip(offset)
                .Take(limit);
        }

        public int CountActiveWithFilters(string search)
        {
            return _dc.Products.Where(p => !p.Deleted)
                .Count(p => search == null
                    || p.Name.ToLower().StartsWith(search.ToLower())
                    || p.Reference.ToLower().StartsWith(search.ToLower())
                 );
        }

        public Product FindOneActive(string reference)
        {
            return _dc.Products.FirstOrDefault(c => !c.Deleted && c.Reference == reference);
        }

        public IEnumerable<Product> FindOrderByQuantity(int limit)
        {
            return _dc.Products.Where(p => !p.Deleted).OrderBy(p => p.Stock).Take(limit);
        }
    }
}
