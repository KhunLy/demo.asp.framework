﻿using GestionStock.DAL.Abstractions.Enums;

namespace GestionStock.DAL.Abstractions.Views
{
    public class OrderStatusRepartition
    {
        public decimal SubTotal { get; set; }
        public int Count { get; set; }
        public OrderStatus Status { get; set; }
    }
}
