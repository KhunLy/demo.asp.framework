﻿using GestionStock.DAL.Abstractions.Entities;
using GestionStock.DAL.Abstractions.Enums;
using GestionStock.DAL.Abstractions.Views;
using System;
using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Interfaces
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        void AddLine(OrderLine line);
        Order FindOneWithOrderLines(string reference);
        IEnumerable<Order> FindOrderByDateWithFilters(int offset, int limit, string reference, string customerRef, List<OrderStatus> orderStatuses, DateTime? fromDate, DateTime? toDate);
        IEnumerable<OrderStatusRepartition> GetCountAndSumGroupByStatuses();
        void RemoveLine(OrderLine line);
        void UpdateLine(OrderLine line, int quantity);
    }
}