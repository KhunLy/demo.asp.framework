﻿using GestionStock.DAL.Abstractions.Entities;

namespace GestionStock.DAL.Abstractions.Interfaces
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User FindOneByEmail(string email);

        void ChangePassword(User u, byte[] encodedPassword);
    }
}
