﻿using System;
using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Interfaces
{
    public interface IRepositoryBase<TEntity> where TEntity : class, new()
    {
        TEntity Add(TEntity entity);
        int Count();
        int CountWhere(Func<TEntity, bool> predicate);
        TEntity Delete(TEntity entity);
        bool Exists(Func<TEntity, bool> predicate);
        IEnumerable<TEntity> Find();
        TEntity FindOne(params object[] key);
        TEntity FindOneWhere(Func<TEntity, bool> predicate);
        IEnumerable<TEntity> FindWhere(Func<TEntity, bool> predicate);
        TEntity Update(TEntity entity);
    }
}