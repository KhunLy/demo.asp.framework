﻿using GestionStock.DAL.Abstractions.Entities;
using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Interfaces
{
    public interface ICustomerRepository: IRepositoryBase<Customer>
    {
        int CountActiveWithFilters(string search = null);
        IEnumerable<Customer> FindActiveOrderByReferenceWithFilters(string search = null, int offset = 0, int limit = 20);
        IEnumerable<Customer> FindAllActiveOrderByReference();
        Customer FindOneActive(string reference);
    }
}