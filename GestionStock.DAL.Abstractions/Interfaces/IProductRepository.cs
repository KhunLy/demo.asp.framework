﻿using GestionStock.DAL.Abstractions.Entities;
using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Interfaces
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        int CountActiveWithFilters(string search);
        IEnumerable<Product> FindActiveOrderByReferenceWithFilters(int offset, int limit, string search);
        Product FindOneActive(string reference);
        IEnumerable<Product> FindOrderByQuantity(int limit);
    }
}