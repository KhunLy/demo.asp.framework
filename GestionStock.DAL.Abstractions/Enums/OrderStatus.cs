﻿namespace GestionStock.DAL.Abstractions.Enums
{
    public enum OrderStatus
    {
        InProgress,
        Pending,
        Closed,
        Canceled
    }
}
