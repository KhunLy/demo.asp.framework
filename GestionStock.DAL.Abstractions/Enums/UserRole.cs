﻿namespace GestionStock.DAL.Abstractions.Enums
{
    public enum UserRole
    {
        Admin,
        Seller,
        Restocker,
    }
}
