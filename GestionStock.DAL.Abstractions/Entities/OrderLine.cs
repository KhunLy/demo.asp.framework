﻿namespace GestionStock.DAL.Abstractions.Entities
{
    public class OrderLine
    {
        public int Id { get; set; }

        public string OrderRef { get; set; }

        public string ProductRef { get; set; }

        public int Quantity { get; set; }

        public decimal? UnitPrice { get; set; }

        public Order Order { get; set; }
        public Product Product { get; set; }
    }
}
