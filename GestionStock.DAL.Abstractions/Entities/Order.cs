﻿using GestionStock.DAL.Abstractions.Enums;
using System;
using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Entities
{
    public class Order
    {
        public string Reference { get; set; } // yymmddxxxx

        public DateTime Date { get; set; }

        public OrderStatus Status { get; set; }

        public string CustomerRef { get; set; }

        public Customer Customer { get; set; }
        public ICollection<OrderLine> OrderLines { get; set; }
    }
}
