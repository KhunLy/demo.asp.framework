﻿using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Entities
{
    public class Customer
    {
        public string Reference { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool Deleted { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
