﻿using GestionStock.DAL.Abstractions.Enums;
using System;

namespace GestionStock.DAL.Abstractions.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public UserRole Role { get; set; }
        public byte[] EncodedPassword { get; set; }
        public Guid Salt { get; set; }
    }
}
