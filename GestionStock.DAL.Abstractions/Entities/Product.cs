﻿using System.Collections.Generic;

namespace GestionStock.DAL.Abstractions.Entities
{
    public class Product
    {
        public string Reference { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Stock { get; set; }

        public decimal Price { get; set; }

        public bool Deleted { get; set; }

        public ICollection<OrderLine> OrderLines { get; set; }
    }
}
